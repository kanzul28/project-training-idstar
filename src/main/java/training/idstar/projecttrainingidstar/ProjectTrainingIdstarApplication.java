package training.idstar.projecttrainingidstar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjectTrainingIdstarApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjectTrainingIdstarApplication.class, args);
	}

}

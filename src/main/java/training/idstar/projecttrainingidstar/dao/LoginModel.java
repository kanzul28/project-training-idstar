package training.idstar.projecttrainingidstar.dao;

import lombok.Data;

@Data
public class LoginModel {

    private String email;

    private String password;
}
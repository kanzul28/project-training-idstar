package training.idstar.projecttrainingidstar.repository.oauth;

import training.idstar.projecttrainingidstar.model.oauth.Client;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ClientRepository extends PagingAndSortingRepository<Client, Long> {

    Client findOneByClientId(String clientId);

}

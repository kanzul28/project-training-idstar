package training.idstar.projecttrainingidstar.service;


import training.idstar.projecttrainingidstar.dao.KaryawanTrainingRequest;

import java.util.Map;

public interface KaryawanTrainingService {

    public Map insert(KaryawanTrainingRequest obj);

    public Map update(KaryawanTrainingRequest obj);

    public Map delete(Long obj);
}

package training.idstar.projecttrainingidstar.service;

import training.idstar.projecttrainingidstar.model.Training;

import java.util.Map;

public interface TrainingService {

    public Map insert(Training obj);

    public Map update(Training obj);

    public Map delete(Long idTraining);
}
